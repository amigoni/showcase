//
//  PostCell.swift
//  showcase
//
//  Created by Leonardo Amigoni on 11/7/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class PostCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var showcaseImage: UIImageView!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var likedImage: UIImageView!
    
    var post: Post!
    var request: Request?
    var likeRef: Firebase!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: "likedTapped:")
        tap.numberOfTapsRequired = 1
        likedImage.addGestureRecognizer(tap)
        likedImage.userInteractionEnabled = true
    }
    
    override func drawRect(rect: CGRect) {
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.clipsToBounds = true
        
        showcaseImage.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(post: Post, image: UIImage?){
        self.post = post
        self.descriptionText.text = post.postDescription
        self.likesLabel.text = "\(post.likes)"
        self.likeRef = Dataservice.ds.REF_USER_CURRENT.childByAppendingPath("likes").childByAppendingPath(post.postKey)

        if post.imageUrl != nil {
            if image != nil {
                self.showcaseImage.image = image
                self.showcaseImage.hidden = false
            } else {
                request = Alamofire.request(.GET, post.imageUrl!).validate(contentType: ["image/*"]).response(completionHandler: { request, response, data, err in
                    
                    if err == nil {
                        //Change to if let
                        let image1 = UIImage(data: data!)!
                        self.showcaseImage.image = image1
                        self.showcaseImage.hidden = false
                        FeedVC.imageCache.setObject(image1, forKey: self.post.imageUrl!)
                    }
                })
            }
            
        } else {
            self.showcaseImage.hidden = true
        }
        
        likeRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if let doesNotExist = snapshot.value as? NSNull {
                //Not liked the post
                self.likedImage.image = UIImage(named: "heart-empty")
            } else {
                self.likedImage.image = UIImage(named: "heart-full")
            }
        })
    }
    
    func likedTapped(sender: UITapGestureRecognizer){
        likeRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if let doesNotExist = snapshot.value as? NSNull {
                self.likedImage.image = UIImage(named: "heart-full")
                self.post.adjustLikes(true)
                self.likeRef.setValue(true)
            } else {
                self.likedImage.image = UIImage(named: "heart-empty")
                self.post.adjustLikes(false)
                self.likeRef.removeValue()
            }
            self.likesLabel.text = "\(self.post.likes)"
        })
    }

}
