//
//  Constants.swift
//  showcase
//
//  Created by Leonardo Amigoni on 11/5/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import Foundation
import UIKit

let SHADOW_COLOR: CGFloat = 157.0 / 255.0

//Keys
let KEY_UID = "uid"

//Segues
let SEGUE_LOGGED_IN = "loggedIn"

//Status Codes
let STATUS_ACCOUNT_NOT_EXIST = -8