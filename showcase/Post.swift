//
//  Post.swift
//  showcase
//
//  Created by Leonardo Amigoni on 11/9/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import Foundation
import Firebase

class Post {
    private var _postDescription: String!
    private var _imageUrl: String?
    private var _likes: Int!
    private var _userName: String!
    private var _postKey: String!
    private var _postRef: Firebase!
    
    var postDescription: String {
        return _postDescription
    }
    
    var imageUrl: String? {
        return _imageUrl
    }
    
    var likes: Int {
        return _likes
    }
    
    var userName: String {
        return _userName
    }
    
    var postKey: String{
        return _postKey
    }
    
    //When I create a post
    init(postDescription: String, imageUrl: String?, userName: String){
        self._postDescription = postDescription
        self._imageUrl = imageUrl
        self._userName = userName
    }
    
    //When a post is downloaded
    init(postKey: String, dictionary: Dictionary<String, AnyObject>){
        self._postKey = postKey
        
        if let likes = dictionary["likes"] as? Int {
            self._likes = likes
        }
        
        if let imageUrl = dictionary["imageUrl"] as? String! {
            self._imageUrl = imageUrl
        }
        
        if let postDescription = dictionary["description"] as? String! {
            self._postDescription = postDescription
        }
        
        self._postRef = Dataservice.ds.REF_POSTS.childByAppendingPath(self._postKey)
    }
    
    func adjustLikes(addLike: Bool){
        if addLike == true{
            _likes = _likes + 1
        } else {
            _likes = _likes - 1
            if _likes < 0 {
                _likes = 0
            }
        }
        
        self._postRef.childByAppendingPath("likes").setValue(_likes)
    }
    
}